function station,name,h=h,lat=lat
;+
; NAME:
; station
;
; PURPOSE:
; define a structure that gives elevation above the reference
; ellipsoid (in meters) and geodetic latitude (in
; radian) of a given observing site 
;
; CATEGORY: 
; Astronomy
;
; CALLING SEQUENCE:
; site=station('Site_name') ;look in the database
; site=station('Site_name',h=h,lat=[deg,min,sec]) ;just create the
; structure from user inputs h and lat
; 
; INPUTS:
; name: string giving the Site name. <br>
;
; KEYWORD PARAMETERS:
; h: altitude above WGS84 reference ellipsoid in meters
; lat: geodetic latitude either in fractional degrees or as a vector [degree,min,sec]
;
; OUTPUTS:
; 'station' Structure <br>
;           station.h (elevation above the reference ellipsoid) <br>
;           station.lat (geodetic latitude in rad) <br>
;           station.name (string) name of the station
;
;PROCEDURE:
;
; define station structure for station in the database or use keywords to create user defined
; station structure<br><br>
;
; CALLS:  <a  href="http://idlastro.gsfc.nasa.gov/ftp/pro/astro/ten.pro"> ten.pro </a> from IdlAstro lib
;
; EXAMPLE:
;
; calern=station('Calern') ;provide geodetic latitude and elvation above the ellipsoid
; calern2=station('Calern2',h=1271.d0,lat=[43.D0,44.D0,53.D0]) ;astronomic latitude and  elevation above see level
; ;Print various radius and show difference in meters
; print,round(curvature_radius(calern.lat, calern.h))
; print,round(curvature_radius(calern2.lat,calern2.h)-curvature_radius(calern.lat, calern.h))
; 
; 6367308     6389694     6378491     6369278
; -56         -53         -55         -51
;
; MODIFICATION HISTORY:
; T. Corbard 02/2013 corbard@oca.eu <br>
; TC Oct. 2015 Added call to IDLAstro 'ten'. Allows
; user defined station with optional keywords h and lat  <br>
; Replace the astronomical latitude by the geodetic latitude of Calern <br>
; Replace the altitude above sea level by the altitude above the WGS84
; reference ellipsoid for Calern. 
;-

if((N_elements(h) ne 0)  && (N_elements(lat) ne 0)) then begin
    sta={station, h:h, lat:ten(lat)*!DPI/180.D0, name:name}
endif else begin
    if((N_elements(h) ne 0) || (N_elements(lat) ne 0)) then message,'Both h and lat must be provided'
    
    CASE STRLOWCASE(name) OF
;'calern':sta={station, h:1274.2423d0,lat:(43.D0+(44.D0+((55.D0)/60.D0))/60.D0)*!DPI/180.D0, name:'Calern'}
        'calern':sta={station, h:1323.d0, lat:ten([43.D0,45.D0,7.D0])*!DPI/180.D0, name:'Calern'}
        ELSE : message, 'Unknown station, please use keywords h= and lat=' 
    ENDCASE
END

return,sta

END
