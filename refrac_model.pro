function refrac_model,name
;+
; NAME:
; refrac_model
;
; PURPOSE:
; returns a structure that defines a refraction model
;
; CATEGORY: 
; Astronomy
;
; CALLING SEQUENCE:
; refraction=refrac_model('Model_name')
;
; INPUTS:
; name: string giving the refraction model name (case insensitive).
;        Available models are : <br>
;        'NAO63Ciddor'   <br>
;        'tan5Baldini'  <br>
;        'tanCiddor'     <br>
;        'tan5Ciddor'    <br>
;        'erfcCiddor'    <br>
;        'CassiniCiddor' <br>
;        'Laplace' (same as 'tan3Ciddor') <br> 
;        'Laclare' (same as 'tan5Baldini')     <br><br><br>
;
; the first chararters give the type of refraction model: <br><br>
;
;       'NAO63'    <br>Exact evaluation of the refraction integral from
;                  standard atmospheric model.
;                  N.A.O Technical Notes 59 and 63 and a paper by Standish and Auer
;                  "Astronomical Refraction: Computational Method for
;                  all Zenith Angles". Modified to take Cidor (1996) modern
;                  air index calculation (see <a href="refrac_all_p.html#aref" >aref.pro </a> ) <br><br>
;
;       'Laclare'  <br>Code provided by F. Laclare for Calern Observatory
;       (see <a href="refrac_all_p.html#refrac_laclare" >refrac_laclare.pro </a> )
;                  <br><br>
;        or one of the formulae involving only the atmospheric air index and
;        reduced height (see <a href="refrac_all_p.html#refrac_for" >refrac_for.pro </a> ) :<br><br>
;       'tan'  One term expansion R=k*tan(z) <br>
;
;       'tan3' Laplace Formula (2 terms expansion in odd powers of
;       tan(z)) <br>
;
;       'tan5' 3 terms expansion (Danjon 1980, derived from erfc formula) <br>
;
;       'erfc' (error function, Danjon 1980, assuming exponential
;              law for variation of density with height) <br>
;
;       'Cassini' Cassini's model for homogeneous atmosphere <br><br>
;
; The following characters give the type of model for air index (see <a href="air_index.html#air_index" >air_index.pro </a> ) (not used if type='NAO63' or
; type='Laclare') <br><br>
;
;          'Baldani' (Chollet 1991 with Barel and Sears (1939) for
;                     refractivity and IUGG 1963 +BDL 75 for pressure,
;                     temperature, humidity dependence) <br><br>
;
;          'Ciddor' Ciddor (1996) modern calculation <br><br>
;
; OUTPUTS:
; structure defining the refraction model 
; (input for procedure refrac_all_p.pro)
;
; EXAMPLE:
; ref_NAO63Ciddor=refrac_model('NAO63Ciddor')
; ref_Laclare=refrac_model('Laclare')
;
; MODIFICATION HISTORY:
; T. Corbard 02/2013 corbard@oca.eu
;
;-

;Check if 'Cidor' is used in place of 'Ciddor'
namelow=STRLOWCASE(name)
p=strpos(namelow,'cidor')
if(p ne -1) then namelow=strmid(namelow,0,p)+'ciddor'+strmid(namelow,p+5,strlen(namelow)-p-4)

CASE namelow OF

'tan3ciddor'   :ref={refrac, type:'tan3'    ,type_rc:'meridian',type_ai:'Ciddor' ,Az:0.D0}
'laplace'      :ref={refrac, type:'tan3'    ,type_rc:'meridian',type_ai:'Ciddor' ,Az:0.D0}
'laclare'      :ref={refrac, type:'Laclare',type_rc:''        ,type_ai:''        ,Az:0.D0}
'nao63ciddor'  :ref={refrac, type:'NAO63'  ,type_rc:'meridian',type_ai:''        ,Az:0.D0}
'tan5baldini'  :ref={refrac, type:'tan5'   ,type_rc:'meridian',type_ai:'Baldini' ,Az:0.D0}
'tanciddor'    :ref={refrac, type:'tan'    ,type_rc:'meridian',type_ai:'Ciddor'  ,Az:0.D0}
'tan5ciddor'   :ref={refrac, type:'tan5'   ,type_rc:'meridian',type_ai:'Ciddor'  ,Az:0.D0}
'erfcciddor'   :ref={refrac, type:'erfc'   ,type_rc:'meridian',type_ai:'Ciddor'  ,Az:0.D0}
'cassiniciddor':ref={refrac, type:'Cassini',type_rc:'meridian',type_ai:'Ciddor'  ,Az:0.D0}

ELSE : message, 'unknown refraction model: '+name

ENDCASE

return,ref

end
