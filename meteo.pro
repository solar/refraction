function meteo,model=model,T=T,P=P,hum=hum,laps=laps,xc=xc,density=density
;+
; NAME:
; meteo
;
; PURPOSE:
; gives a structure that defines atmospheric conditions
;
; CATEGORY: 
; Atmospheric Physics
;
;
; KEYWORD PARAMETERS:
; model: string for predefined weather conditions <br>
; 'Std' T=15$^\circ$C, P=1013.25 hPa, hum=0 %, laps=6.5 K/Km, xc=450 ppm <br>
; 'Isothermal' T=15$^\circ$C, P=1013.25 hPa, hum=0 %, laps=1.d-12 K/m, xc=450 ppm <br> 
; 'Adiabatic' T=15$^\circ$C, P=1013.25 hPa, hum=0 %, laps=10 K/km, xc=450 ppm <br>
; 'Calern_mean' T=15$^\circ$C, P=875 hPa, hum=50 %, laps=6.5 K/km, xc=380 ppm <br>
; 'Calern_min' T=-10$^\circ$C, P=900 hPa, hum=50 %, laps=6.5 K/km, xc=380 ppm <br>
; 'Calern_max' T=30$^\circ$C, P=850 hPa, hum=50 %, laps=6.5 K/km, xc=380 ppm <br><br>
; T: [K] Temperature
; P: [Pa] Pressure
; hum: [%] in [0-100] humidity
; laps: [K/m] Troposheric laps rate
; xc: [ppm] Carbon dioxid concentration
; density: [kg/m^3] Air density 
; 
; OUTPUTS:
; Structure containing temperature (meteo.T), pressure (meteo.P),
; humidity (meteo.hum), troposheric
; laps rate (meteo.laps) and CO2 concentration (meteo.xc). <br>
; Missing information from input are replaced by standard atmosphere
; values (Ciddor 1996) or by the chosen model if [model=] is set. 
;
; EXAMPLE:
; std_atm=meteo()  => return Ciddor 1996 standard atmosphere parameters.
; atm1=meteo(model='Isothermal',T=293.D0,P=100000.D0) Isothermal model
; except for T and P.
;
; MODIFICATION HISTORY:
; T. Corbard 02/2013 corbard@oca.eu
;
;-

;Standard Atmosphere in Ciddor (1996)
std_atm    ={meteo,T:273.15D0+15.D0,P:101325.D0,hum:0.D0,laps:0.0065D0,xc:450.D0, density:0.D0}
isothermal ={meteo,T:273.15D0+15.D0,P:101325.D0,hum:0.D0,laps:1.D-12  ,xc:450.D0, density:0.D0}
adiabatic=  {meteo,T:273.15D0+15.D0,P:101325.D0,hum:0.D0,laps:0.010D0,xc:450.D0, density:0.D0}

Calern_mean={meteo,T:273.15D0+15.D0,P:87500.D0,hum:50.D0,laps:0.0065D0,xc:380.D0, density:0.D0}
Calern_min= {meteo,T:273.15D0-10.D0,P:90000.D0,hum:50.D0,laps:0.0065D0,xc:380.D0, density:0.D0}
Calern_max= {meteo,T:273.15D0+30.D0,P:85000.D0,hum:50.D0,laps:0.0065D0,xc:380.D0, density:0.D0}

if(N_elements(model) eq 0) then model='Std'


CASE model OF
'Std':atm=std_atm
'Isothermal':atm=isothermal
'Adiabatic':atm=adiabatic
'Calern_mean':atm=Calern_mean
'Calern_min':atm=Calern_min
'Calern_max':atm=Calern_max
ELSE : message, 'unknown meteo: '+model
    
ENDCASE



if(N_elements(T) ne 0) then if(finite(T)) then atm.T=T
if(N_elements(P) ne 0)then if(finite(P)) then atm.P=P
if(N_elements(hum) ne 0) then if(finite(hum)) then atm.hum=hum
if(N_elements(xc) ne 0) then if(finite(xc)) then atm.xc=xc
if(N_elements(laps) ne 0)then if(finite(laps)) then atm.laps=laps

atm.density=atmospheric_density(atm.T-273.15D0, atm.P, atm.hum, atm.xc)
if(N_elements(density) ne 0)then if(finite(density)) then atm.density=density

return,atm

end
