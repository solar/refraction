# REFRACTION

Astronomical Refraction Routines For Solar Astrometry

## Overview
Please refer to the following research note:

T Corbard, R Ikhlef, F Morand, M Meftah, C Renaud, **On the importance of 
astronomical refraction for modern solar astrometric measurements**, 
[Monthly Notices of the Royal Astronomical Society, Volume 483, Issue 3, 
March 2019, Pages 3865–3877, ](https://doi.org/10.1093/mnras/sty3391)

[arXiv:1304.0147 [astro-ph.SR]](https://arxiv.org/abs/1304.0147)

### Example 1: Get the observed zenith distance from the true one and back
```
;Get station latitude and elevation from its name
calern=station('Calern')
;
;Get standard (Ciddor, 1996) atmospheric condition
std_atm=meteo()
;
;Select a refraction model
std_refrac=refrac_model('NAO63Ciddor') ;exact integral calculation from Standard atmosphere
;
;Choose a true zenith distance
zt=70.D0*!DPI/180.D0
;
;Choose a wavelength
lbda=535.7d-9
;
;Get the observed zenith distance
z=zt-refrac_all_p(zt,lbda,calern,std_atm,std_refrac,/true)
print,z*180.D0/!DPI
;
;Get back the true zenith distance
zt=z+refrac_all_p(z,lbda,calern,std_atm,std_refrac)
print,zt*180.D0/!DPI

69.956757
70.000000
```

### Example 2: Compare curvature radius estimates using either geodetic latitude / elvation above the reference ellipsoid or astronomic latitude and elevation above sea level
```
;provide geodetic latitude and elvation above the WGS84 reference ellipsoid
calern=station('Calern') 
;
;astronomic latitude and elevation above sea level
calern2=station('Calern2',h=1271.d0,lat=[43.D0,44.D0,53.D0]) 
;
;Print the difference (in meters) for various curvature radius estimates (see curvature_radius.pro)
print,round(curvature_radius(calern2.lat,calern2.h)-curvature_radius(calern.lat, calern.h))

-56 -53 -55 -51

```
### Example 3: Compare reduced height either assuming ideal gaz law or from full air density calculation
```
;Get average weather conditions at Calern observatory
atm=meteo(model='Calern_mean') 
;
;Get the air density from Ciddor (1996) equations
print,'Air Density: ',atm.density,' kg/m^2',format='(a,d5.3,a)'
g=9.802155492d0 ; m/s^2 Measured gravity acceleration at Calern
print,'Reduced height (Full density calculation): ',round(reduced_height([atm.P,atm.density,g])),' m'
print,'Reduced height (Assuming ideal gaz law ): ',round(reduced_height([atm.T])),' m'

Air Density: 1.054 kg/m^2
Reduced height (full density calculation): 8467 m
Reduced height (assuming ideal gaz law ): 8430 m
```