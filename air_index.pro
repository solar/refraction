;+
; NAME: air_index
;
; PURPOSE: 
; Compute ambiant air refractive index following
; either Baldini (1963) (IUGG 1963 adopted standard) or Ciddor (1996) (IAG
; 1999 adopted standard)
;
; CATEGORY: 
; Atmospheric Physics
;
; CALLING SEQUENCE:
; nobs=air_index(Ta,P,hum,lbda,type[,xc=xc])
;
; INPUTS:
; Ta:    [K] temperature
; P:     [Pa] Pressure
; hum:   humidity [%] in [0,100]
; lbda:  wavelength [m]
; type: 'Baldini' or 'IUGG1963'    <br>
;       'Ciddor' or 'IAG1999'
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
; xc: [ppm] Carbon dioxyde concentration (used in 'Ciddor' model only). <br>
;     380 ppm is taken by default (if xc is not set) in 'Ciddor' model. <br>
;     300 ppm is assumed in 'Baldani' model.
;
; OUTPUTS:
; Refractive index $n$ of ambiant air at given wavelength.
;
; PROCEDURE:
; 
; +Model 'Baldini'/'IUGG1963' (IUGG, 1963, depreciated) <br><br>
;
;\begin{equation}
;T_0=273.15\, \mathrm{K}\, ,P_0=101325\, \mathrm{Pa}\, ,x_c=300\, \mathrm{ppm}
;\end{equation}
; \begin{equation}
; n(T,P,f_h,\lambda)-1={T_0 \over T} \left\{(n_0(\lambda)-1) {P
; \over P_0} - 4.13\, 10^{-10}\ p(f_h,T)\right\} \,\, [1, 2]
; \end{equation}
;\begin{equation}
;n_0(\lambda)-1=\left\{2876.04+{16.288 \over
;(10^6\lambda)^2}+{0.136\over (10^6\lambda)^4}\right\}\ 10^{-7} \,\,[3]
;\end{equation}
;\begin{equation}
;p(f_h,T)=f_h\ 6.1075\, 10^2\ e^{7.292\,
;10^{-2}(T-T_0)-2.84\,10^{-4}(T-T_0)^2} \,\, [4,5]
;\end{equation}
;  [1] Baldini, A. A. 1963, GIMRADA Research Note, 8 <br>
;  [2] IUGG (International Union of Geodesy and Geophysics), 1963, Resolutions,
;  13th General Assembly, 19-31 August 1963, Berkeley, California, USA.
;  Bulletin Geodesique, 70, 390 <br> 
;  [3] Barrel, H. & Sears, J. E. 1939, Phil. Trans. R. Soc., A238, p. 52, 
;  equation (7.7) for f=0, t=0, p=760 mmHg
;  (assumed xc=300 ppm (0.03%) )  <br>
;  [4] Chollet F. 1981, PhD thesis, Universite Pierre et Marie Curie (Paris VI)  <br>
;  [5] Annuaire du Bureau Des Longitudes, 1975 <br><br>
; +Model 'Ciddor'/'IAG1999' (IAG adopted standard, 1999) <br><br>
;  [6] Ciddor, P.E., 1996, "Refractive index of air: new equations for the visible and near infrared", Applied Optics LP, vol. 35, Issue 9, p.1566  <br>
;  [7] <a href="http://iag.dgfi.tum.de/fileadmin/IAG-docs/IAG_Resolutions_1999.pdf" > IAG
;  (International Association of Geodesy), 1999, Resolution 3, 22nd
;  GeneralAssembly, 19-30 July 1999,Birmingham, U.K.</a>
; <br> CALLS:
;        $SSW/optical/gen/idl/util/refractivity.pro (SolarSoft library)
;
; EXAMPLE:
;
; MODIFICATION HISTORY:
; Written by: T. Corbard 06/2012 (corbard@oca.eu)
;
;-
function air_index,Ta,P,hum,lbda,type,xc=xc

COMPILE_OPT IDL2

IF (N_ELEMENTS(xc) NE 1) THEN xc = 380.d0

T0=273.15D0
t=Ta-T0
if(type eq 'Cidor') then type='Ciddor'
case 1 of
(type eq 'Baldini') or (type eq 'IUGG1963'): BEGIN
    P0=101325.D0 ;Pa
    lbd=lbda*1.d6              ;micrometer
    ;al=1.D0/273.15d0;0.00367d0
    lbd2=lbd*lbd
    lbd4=lbd2*lbd2
    
    n0m1=1.D-7*(2876.04D0+16.288D0/lbd2+0.136D0/lbd4) ;[3]
    
    hsat=4.581D0*exp(7.292D-2*t-2.84D-4*t^2) ;mmHg [4, 5]
    eh=(hum*hsat/100.D0)
    ;n=1.D0+((1.D0/(al*Ta))*(n0m1*(P/P0)-5.5D-8*eh)) 
    n=1.D0+((T0/Ta)*(n0m1*(P/P0)-5.5D-8*eh)) ;[1, 2]
END
(type eq 'Ciddor') or (type eq 'IAG1999'):BEGIN
    wavelength=lbda*1.d9
    nm1=refractivity(wavelength, t, P, hum,xc) ;[6,7]
    n=nm1+1.D0
END
ELSE: message,'Unknown type: '+type
ENDCASE
return,n
end
