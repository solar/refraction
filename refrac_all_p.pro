;+
; NAME:
; refrac_all_p.pro
; 
;
; PURPOSE:
; Compute astronomical refraction for given observed or true zenith distances.
; It uses either approximate formulae for calculating the refaction
; integral knowing the weather condition at observer (valid for low
; zenith distances) or performs the full integral calculation assuming 
; a standard atmosphere together with the observed ground weather
; conditions. See <a href="refrac_all_p.html#refrac_all_p" >refrac_all_p.pro </a>
; 
;
;-


function refi,R,N,DNDR
;+
; NAME:
; refi
;
; PURPOSE:
; utility function for aref.pro
;
; CATEGORY:
; utility
;
; CALLING SEQUENCE:
; ri=refi(R,N,DNDR)
;
; INPUTS:
; R:   The current distance from the centre of the Earth,metres
; N:  The refractive index at R
; DNDR:  The rate the refractive index is changing at R
;
; OUTPUTS:
; Refractive index
;
; MODIFICATION HISTORY:
; Vax 11/750 1984 August Catherine Hohenkerk. <br>
; Adapted from fortran T. Corbard 06/2012 (corbard@oca.eu)
;
;-

  return,R*DNDR/(N + R*DNDR)
end

pro  atmostro,R0,T0,A,R,T,N,DNDR
;+
; NAME: 
; atmostro
;
; PURPOSE:
; Atmosphere of the troposphere, utility procedure for aref.pro
;
; CATEGORY: 
;           Atmospheric Physics
;
; CALLING SEQUENCE:
;           atmostro,R0,T0,A,R,T,N,DNDR
;
; INPUTS:
; R0:  The height of the observer from the centre of the Earth
; T0:  The temperature at the observer in degres K
; A: A[10] Constants defined at the observer
; R:   The current distance from the centre of the Earth,metres
; 
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;
; OUTPUTS:
; T:  The temperature at R in degrees K
; N:  The refractive index at R
; DNDR:  The rate the refractive index is changing at R
;
; OPTIONAL OUTPUTS:
;
; COMMON BLOCKS:
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;
; PROCEDURE:
;
; EXAMPLE:
;
; MODIFICATION HISTORY:
; Vax 11/750 1984 August Catherine Hohenkerk. <br>
; Adapted from fortran T. Corbard 06/2012 (corbard@oca.eu)
;
;-

;	  IMPLICIT DOUBLE PRECISION (A-H,O-Z)
;	  DOUBLE PRECISION N,A(10)
;C
T = T0 - A[1]*(R-R0)
TT0 = T/T0
TT01 = TT0^(A[3]-2.0D0)
TT02 = TT0^(A[4]-2.0D0)
N = 1.0D0 + ( A[7]*TT01 - A[8]*TT02 )*TT0
DNDR = -A[9]*TT01 + A[10]*TT02
;C
END
	
pro atmosstr,RT,TT,NT,A,R,N,DNDR
;+
; NAME: 
; atmosstr
;
; PURPOSE:
; Atmosphere of the stratosphere, utility procedure for aref.pro 
;
; CATEGORY: 
; Atmospheric Physics
;
; CALLING SEQUENCE:
; atmosstr,RT,TT,NT,A,R,N,DNDR
;
; INPUTS:
; RT: D The height of the tropopause from the centre of the
;       Earth in metres
; TT:  The temperature at the tropopause in degrees K
; NT: The refractive index at the tropopause
; A:  Constant of the atmospheric model = G*MD/R
; R:  The current distance from the centre of the Earth in metres
;
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;
; OUTPUTS:
; N:  The refractive index at R
; DNDR:  The rate the refractive index is changing at R
;
; OPTIONAL OUTPUTS:
;
; COMMON BLOCKS:
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;
; PROCEDURE:
;
; EXAMPLE:
;
; MODIFICATION HISTORY:
; Vax 11/750 1984 August Catherine Hohenkerk. <br>
; Adapted from frotran T. Corbard 06/2012 (corbard@oca.eu)
;
;-

;	IMPLICIT DOUBLE PRECISION (A-H,O-Z)
;	  DOUBLE PRECISION N,NT
;C
B = A/TT
N = 1.0D0 + (NT - 1.0D0)*EXP(-B*(R-RT))
DNDR = -B*(NT-1.0D0)*EXP(-B*(R-RT))
;C
END

function  aref,Z0,H0,T0,P0,UPS,WL,PH,AS,rc,EPS,xc=xc
;+
; NAME: 
; aref
;
; PURPOSE:
; A subroutine to calculate bending astronomical refraction
;
; CATEGORY: 
; Astronomy
;
; CALLING SEQUENCE:
; aref,Z0,H0,T0,P0,UPS,WL,PH,AS,rc,EPS
;
; INPUTS:
; Z0:  The observed zenith distance of the object in degrees
; H0:  The height of the observer above sea level in metres
; T0:  The temperature at the observer in degrees K
; P0:  The pressure at the observer in millibars
; UPS: The relative humidity at the observer (UPS in ]0,1[ )
; WL:  The wavelength of the light at the observer in micrometres
; PH:  The latitude of the observer in degrees
; AS:  The temperature lapse rate in degrees K/metre in the
;       troposphere, the absolute value is used
; rc:  Earth radius of curvature at observer in meters 
; EPS:  The precision required in seconds of arc
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
; xc: Carbon dioxyd concentration [ppm] (default 450)
;
; OUTPUTS:
; The refraction at the observer in degrees
;
; OPTIONAL OUTPUTS:
;
; COMMON BLOCKS:
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;
; PROCEDURE:
; The method is based on
; <a href="http://astro.ukho.gov.uk/data/tn/naotn63.pdf">N.A.O Technical Notes 59 and 63</a> and a paper by Standish and Auer. <br>
; <a href="http://iopscience.iop.org/article/10.1086/301325/pdf">Auer L., & Sandish, Astronomical Refraction: Computational Method
; for all Zenith Angles,E. M. 2000, ApJ, 119, 2472</a> <br><br>
;
;CALLS: <a href="refrac_all_p.html#refi" >refi </a>,
; <a href="refrac_all_p.html#atmostro" >atmostro </a>,
; <a href="refrac_all_p.html#atmosstr" >atmosstr </a>   
;
; EXAMPLE:
;
; MODIFICATION HISTORY:
; Vax 11/750 1984 September Catherine Hohenkerk HMNAO. <br>
; Adapted from fortran T. Corbard 06/2012 (corbard@oca.eu). <br>
; Barrel & Sears (1939) equation replaced by Ciddor (1996).
;
;-
COMPILE_OPT IDL2
;	IMPLICIT DOUBLE PRECISION (A-H,O-Z)
;	DOUBLE PRECISION MD,MW,N,N0,NT,NTS,NS,A(10)
;C
          GCR=8314.36D0
          MD=28.966D0
          MW=18.016D0
          S=rc;S=6378120.0D0
          GAMMA=18.36D0
          HT=11000.0D0
          HS=80000.0D0
          DGR=0.01745329252D0
          Z2=11.2684D-06
;C
;C Set up parameters defined at the observer for the atmosphere
;C
	  GB= 9.784D0*(1.0D0 - 0.0026D0*COS(2.0D0*PH*DGR) - 0.00000028D0*H0)
;         --------- Barrel & Sears (1939)---------------
	 ; Z1 = (287.604D0 + 1.6288D0/(WL*WL) + $
          ;      0.0136D0/(WL*WL*WL*WL))*(273.15D0/1013.25D0)*1.0D-06
          ;Change to Cidor (1996) -------------------------------------------
          b1=5.792105d-2
          b2=1.67917d-3
          c1=238.0185d0
          c2=57.362d0
          Z1=(b1/(c1-1.D0/(WL*WL))+b2/(c2-1.D0/(WL*WL)))*((273.15D0+15.D0)/1013.25D0)
          if(N_elements(xc) ne 0) then Z1=Z1*(1.D0+0.534d-6*(xc-450.D0))
          ;-------------------------------------------------------------------
          A=dblarr(11) ;A[0] not used to keep with original Fortran code
	  A[1] = ABS(AS)
	  A[2] = (GB*MD)/GCR
	  A[3] = A[2]/A[1]
	  A[4] = GAMMA
	  PW0 = UPS*(T0/247.1D0)^A[4]
	  A[5] = PW0*(1.0D0 - MW/MD)*A[3]/(A[4]-A[3])
	  A[6] = P0 + A[5]
	  A[7] = Z1*A[6]/T0
	  A[8] = ( Z1*A[5] + Z2*PW0)/T0
	  A[9] = (A[3] - 1.0D0)*A[1]*A[7]/T0
	  A[10] = (A[4] - 1.0D0)*A[1]*A[8]/T0
;C
;C At the Observer
;C
	  R0 = S + H0
	  atmostro,R0,T0,A,R0,T0O,N0,DNDR0
	  SK0 = N0 * R0 * SIN(Z0*DGR)
	  F0 = refi(R0,N0,DNDR0)
	  
	  
;C       
;C At the Tropopause in the Troposphere
;C
	  RT = S + HT
	  
	  atmostro,R0,T0,A,RT,TT,NT,DNDRT
	  ZT = ASIN(SK0/(RT*NT))/DGR
	  FT = refi(RT,NT,DNDRT)
	  
	  
;C
;C At the Tropopause in the Stratosphere
;C
	  atmosstr,RT,TT,NT,A[2],RT,NTS,DNDRTS
	  ZTS = ASIN(SK0/(RT*NTS))/DGR
	  FTS = refi(RT,NTS,DNDRTS)
;C
;C At the stratosphere limit
;C
	  RS = S + HS
	  atmosstr,RT,TT,NT,A[2],RS,NS,DNDRS
	  ZS = ASIN(SK0/(RS*NS))/DGR
	  FS = refi(RS,NS,DNDRS)
;C
;C Integrate the refraction integral in the troposhere and stratosphere
;C ie Ref = Ref troposhere + Ref stratopshere
;C
;C Initial step lengths etc
;C
	  REFO = -999.999
	  IS = 16
	  FOR K = 1,2 DO BEGIN
              ISTART = 0
              FE = 0.0D0
              FO = 0.0D0
;C
              IF (K EQ 1) THEN BEGIN
                  H = (ZT - Z0)/DOUBLE(IS)
                  FB = F0
                  FF = FT
              ENDIF ELSE IF (K EQ 2) THEN BEGIN
                  H = (ZS - ZTS )/DOUBLE(IS)
                  FB = FTS
                  FF = FS
              ENDIF
              
              IN = IS - 1
              IS = IS/2
              STEP = H
              branch: 	

              FOR I = 1,IN DO BEGIN
                  IF (I EQ 1 AND K EQ 1) THEN BEGIN
                      Z = Z0 + H
                      R = R0
                  ENDIF ELSE IF (I EQ 1 AND K EQ 2) THEN BEGIN
                      Z = ZTS + H
                      R = RT
                  ENDIF ELSE Z = Z + STEP                    
;C
;C Given the Zenith distance (Z) find R
;C
                  RG = R
                  FOR J = 1,4 DO BEGIN
                      IF (K EQ 1) THEN BEGIN
                          atmostro,R0,T0,A,RG,TG,N,DNDR
                      ENDIF ELSE IF (K EQ 2) THEN BEGIN
                          atmosstr,RT,TT,NT,A[2],RG,N,DNDR
                      ENDIF
                    
                      RG = RG - ( (RG*N - SK0/SIN(Z*DGR))/(N + RG*DNDR) )
                  ENDFOR
                  R = RG
;C
;C Find Refractive index and Integrand at R
;C
                  IF (K EQ 1) THEN BEGIN
                      atmostro,R0,T0,A,R,T,N,DNDR
                  ENDIF ELSE   IF (K EQ 2) THEN BEGIN
                      atmosstr,RT,TT,NT,A[2],R,N,DNDR
                  ENDIF
                  
;C
                  F = refi(R,N,DNDR)
;C
                  IF (ISTART EQ 0 AND (I MOD 2) EQ 0) THEN BEGIN
                      FE = FE + F
                  ENDIF ELSE BEGIN
                      FO = FO + F
                  ENDELSE
              ENDFOR
;C
;C Evaluate the integrand using Simpson's Rule
;C
              REFP = H*(FB + 4.0D0*FO + 2.0D0*FE + FF)/3.0D0
;C
              IF (ABS(REFP-REFO) GT 0.5D0*EPS/3600.0D0) THEN BEGIN
                  IS = 2*IS
                  IN = IS
                  STEP = H
                  H = H/2.0D0
                  FE = FE + FO
                  FO = 0.0D0
                  REFO = REFP
                  IF (ISTART EQ 0) THEN ISTART = 1
                  GOTO,  branch
              ENDIF
              IF (K EQ 1) THEN REFT = REFP
          ENDFOR
;C
          REF = REFT + REFP
          RETURN,REF
      END


	




function refrac_laclare,z,lbd,t,hum,Pobs,thg=thg
;+
; NAME: 
; refrac_laclare
;
; PURPOSE: 
; Estimates Atmospheric refraction at Calern Observatory
; (lat=43$^\circ$44'55", h=1.2742423 km)
;                                                                    
; CATEGORY: 
; Astronomy
;
; CALLING SEQUENCE: 
; R=refrac(z,lbd,t1,hum,Pobs,thg)
;
; INPUTS:
; z: [fractional degree] Observed Zenital distance
; lbd: [10^-6 m]  Wavelength
; t: [Celcius] : Air temperature
; hum: [%]  Humidity percent
; Pobs: [mm]  Barometric Pressure
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
; thg: [Celcius] :  Barometer temperature, if not provided Pobs is
; assumed to be already reduced for gravity and thermal expansion
;
; OUTPUTS: 
; Refraction in arc seconds
; 
; OPTIONAL OUTPUTS:
;
; COMMON BLOCKS:
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;
; PROCEDURE:
; Using Laplace formula (Danjon 1958) <br>
;  [1] DANJON A. Astronomie g\'en\'erale 1959 Semac Paris <br>
;  [2] Annuaire du BDL 1975. <br>
;  [3] Joschi Ch, Mueller I, in "Present and future of the
;          Astronomical Refraction Investigations. Edited by TELEKY
;          G. 1974 BEOGRAD p 83. <br>
;  [4] Chollet PhD thesis 1981. 
;
; EXAMPLE:
;
; MODIFICATION HISTORY:
; Formula and code Provided by F. Laclare.  
; Thierry Corbard (Corbard@oca.eu) 05/2012
;
;-
COMPILE_OPT IDL2

T0=273.15D0
P0=760.D0

;mulitpying by d2r  convert from deg to rad
d2r=!DPI/180.D0

;station altitude [km]
h=1.2742423

;station latitude [deg]
phi=43.D0+(44.D0+((55.D0)/60.D0))/60.D0

c2phi=cos(2.D0*phi*d2r)
tgz0=tan(z*d2r)

lbd2=lbd*lbd
lbd4=lbd2*lbd2

;Absolute temperature [K]
Ta=T0+t

;Air index for 0 Celsius and 760 mm Hg  [3]
n=1.D0+1.D-7*(2876.04D0+16.288D0/lbd2+0.136D0/lbd4)


if(n_elements(thg) ne 0) then begin
;Reduced Atmospheric Pressure
;Cholleet 1981 erroneously gave 2.64d-4 for the first coef
    P=Pobs*(1.D0-2.64D-3*c2phi-1.96D-7*h-1.63D-4*thg)
endif else begin
    P=Pobs
endelse

;Pression de vapeur saturante de l'eau pour -15<t<25 [2]
hsat=4.581D0*exp(7.292D-2*t-2.84D-4*t^2)

;pression partielle de la vapeur d'eau
eh=(hum*hsat/100.D0)

;alpha0=n0-1  n0 air index for temperature Ta, pressure P, humidity hum
alpha0=(n-1.D0)*(T0/Ta)*(P/P0)-5.5D-8*(T0/Ta)*eh
;print,'Air index Laclare:' ,alpha0+1.D0

;Ratio between the atmospheric height at constant pressure and Earth 
;curvature radius at observer position.
;For france [4]:
;beta0=4.5054D-6*Ta
rho0=1.293d0 ;kgm^-3
g0=9.80665 ;ms^-2
rc=6367512.D0 ;m
l=101325.D0/(rho0*g0)
beta0=(l/rc)*Ta/T0


;REF=A*z1*((1.D0-b)-(b-A/2.D0)*z2+3*(b-A/2.D0)^2*z4)

A=(alpha0/2.D0-beta0)*tgz0*tgz0

REF=alpha0*tgz0*(1.D0-beta0+A+3*A*A)

return, REF/d2r*3600.D0

end

function refrac_for,n,l,rc,z,type
;+
; NAME: 
; refrac_for
;
; PURPOSE: 
; Computes refraction using various formulae involving only
; the air index $n$ for local atmospheric conditions and the ratio
; between the height of the equivalent homogeneous atmosphere $\ell$ and the
; earth radius of curvature  $r_c$ at observer position.
;\begin{equation}\alpha(T,P,f_h,\lambda)=n(T,P,f_h,\lambda)-1 
;\end{equation}
;\begin{equation}\beta(T,h,\varphi)=\ell(T)/r_c(\varphi,h)
;\end{equation}
;
; CATEGORY: 
; Astronomy
;
; CALLING SEQUENCE:
; R=refrac_for(n,l,rc,z,type)
;
; INPUTS:
; n: air index for local atmospheric conditions
; l: [m]  height of the equivalent homogeneous atmosphere
; rc: [m] earth radius of curvature  at observer position
; z: Scalar or vector, observed topocentric zenith angle[s]
; type: 
;       'tan'  One term expansion R=k*tan(z) <br>
;\begin{equation}R(z,\lambda,P,T,f_h,h,\varphi)=\alpha(1-\beta) \tan(z)
;\end{equation}
;       'tan3' Laplace Formula (2 terms expansion in odd power of
;       tan(z)) <br>
;\begin{equation}R(z,\lambda,P,T,f_h,h,\varphi)=\alpha(1-\beta) \tan(z)-\alpha(\beta-{\alpha \over 2}) \tan^3(z) 
;\end{equation}
;       'tan5' 3 terms expansion (Danjon 1980, derived from erfc
;       formula) <br>
;\begin{equation}R(z,\lambda,P,T,f_h,h,\varphi)=\alpha(1-\beta) \tan(z)-\alpha(\beta-{\alpha \over 2}) \tan^3(z)+3\alpha\left(\beta-{\alpha \over 2}\right)^2 \tan^5(z) 
;\end{equation}

;       'erfc' (error function, Danjon 1980, assuming exponential
;              law for variation of density with height) <br>
;\begin{equation} 
;R=\alpha \left({{2-\alpha}\over
;{\sqrt{2\beta-\alpha}}}\right)\sin(z)\
;\Psi\left({{\cos(z)}\over{\sqrt{2\beta-\alpha}}}\right)
;\end{equation}
;\begin{equation}
;\Psi(x)=e^{x^2}\int_x^{\infty}e^{-t^2}dt={{\sqrt{\pi}}\over{2}}e^{x^2}\left(1-\mathrm{erf}(x)\right)
;\end{equation}
;       'Cassini' Cassini's model for homogeneous atmosphere
;\begin{equation}
;R=\mathrm{asin}\left({{n\
;r_c\sin(z)}\over{r_c+\ell}}\right)-\mathrm{asin}\left({{r_c\sin(z)}\over{r_c+\ell}}\right)
;\end{equation}
; 
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;
; OUTPUTS: 
;  Refraction in radian (same dimension as z). 
;  On exit z+R is (are) the true zenith angle(s)
;
; OPTIONAL OUTPUTS:
;
; COMMON BLOCKS:
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;
; PROCEDURE:
;
; EXAMPLE:
;
; MODIFICATION HISTORY:
; T. Corbard 06/2012 (Corbard@oca.eu)
;
;-
COMPILE_OPT IDL2

;d2r=!DPI/180.D0

alpha=n-1.D0 ;refractivity
beta=l/rc
k=alpha*(1.D0-beta)
a=beta-alpha/2.D0

case type of

'tan': BEGIN
    tz=tan(z)
    R=k*tz
END
'tan3': BEGIN
    tz=tan(z)
    ;R=k*tz-alpha*a*tz^3
    R=tz*(k-(alpha*a)*tz*tz)
END
'tan5': BEGIN
    tz=tan(z)
    ;R=k*tz-alpha*a*tz^3+3.D0*alpha*a^2*tz^5
    atz2=a*tz*tz
    R=tz*(k-alpha*atz2*(1.D0-3.D0*atz2))
    
END
'erfc': BEGIN
    sz=sin(z)
    cz=cos(z)
    c=sqrt(2.D0*a)
    x=cz/c
    R=alpha*(2.D0-alpha)/c*sz*sqrt(!DPI)/2.D0*erfcx(x)
END
'Cassini': BEGIN
    sz=sin(z)
    x=rc*sz/(rc+l)
    R=asin(n*x)-asin(x)
END

ENDCASE

return,R  ;rad
end
    


function refrac_all,z,h,T,P,hum,lbd,lat,type,type_ai,type_rc,laps=laps,xc=xc,Az=Az,true=true
;+
; NAME: 
; refrac_all
;
; PURPOSE: 
; Computes astronomical refraction for various models,
; atmospheric condition, wavelength and for any observed or true
; zenith distance.
;          
; CATEGORY: 
; Astronomy
;
; CALLING SEQUENCE:
; R=refrac_all(z,h,T,P,hum,lbd,lat,type,type_ai,type_rc[,laps=laps][,xc=xc][,Az=Az][,true=true])
;
; INPUTS:
; z: [rad] Observed zenith angle unless optional input 'true=' 
;    is set and non-zero in which case z is the true zenith angle
; h: [m] Observer elevation above sea level
; T: [K] Temperature at observer
; P: [Pa] Pressure at observer
; hum: [%] Humidity at observer (in [0-100])
; lbd: [m] Wavelength
; lat: [rad] Observer's latitude (not used if type='Laclare')
; type: Type of refraction model  <br><br>
;
;       'NAO63'    <br>Exact evaluation of the refraction integral from
;                  standard atmospheric model.
;                  N.A.O Technical Notes 59 and 63 and a paper by Standish and Auer
;                  "Astronomical Refraction: Computational Method for
;                  all Zenith Angles". Modified to take Cidor (1996) modern
;                  air index calculation (see <a href="refrac_all_p.html#aref" >aref.pro </a>) <br><br>
;
;       'Laclare'  <br>Code provided by F. Laclare for Calern Observatory
;       (see <a href="refrac_all_p.html#refrac_laclare" >refrac_laclare.pro </a>)
;                  This is equivalent to:
;                   type='tan5',type_ai='Baldini',type_rc='meridian',lat=(43+(44+((55)/60.d0))/60.D0)*!DPI/180.D0 <br><br>
;        or one of the formulae involving only the atmospheric air index and
;        reduced height (see <a href="refrac_all_p.html#refrac_for" >refrac_for.pro </a>) :<br><br>
;       'tan'  One term expansion R=k*tan(z) <br>
;
;       'tan3' Laplace Formula (2 terms expansion in odd powers of
;       tan(z)) <br>
;
;       'tan5' 3 terms expansion (Danjon 1980, derived from erfc formula) <br>
;
;       'erfc' (error function, Danjon 1980, assuming exponential
;              law for variation of density with height) <br>
;
;       'Cassini' Cassini's model for homogeneous atmosphere <br>
;
; type_ai: Type of model for air index (see <a href="air_index.html#air_index" >air_index.pro </a>) (not used if type='NAO63' or
; type='Laclare') <br><br>
;
;          'Baldani' (Chollet 1991 with Barel and Sears (1939) for
;                     refractivity and IUGG 1963 +BDL 75 for pressure,
;                     temperature, humidity dependence) <br><br>
;
;          'Ciddor' Ciddor (1996) modern calculation <br><br>
;
; type_rc: Type of model for radius of curvature at observer position
;           (see curvature_radius.pro) (not used if  type='Laclare')  <br><br>
;
;          'meridian' radius of curvature in the N-S meridian  <br>
;          'primevertical' Curvature in prime vertical  <br>
;          'meancurv' radius of curvature averaging over all
;          directions <br>
;          'dist2center' distance from geocenter <br>
;          'Az' (only if optional input Az is defined) radius of
;          cruvature for Azimuth angle Az <br>
;
; KEYWORD PARAMETERS:
;laps: The temperature lapse rate in degrees K/metre in the
;       troposphere [K/m]  (used only for type='NAO63', default 0.0065 K/m)
;
; xc: CO2 content [ppm]   (used only if type_ai='Cidor' (xc=300ppm is
;                          assumed in 'Baldani' model and xc=450ppm is
;                          used by default in Cidor model) 
;
; Az: Object Azimuth [rad] (used only if type_rc='Az' is selected,
;                           default =0 )
;
; true: [arcsec] If nonzero it indicates that z is the true zenith angle
;        and its value is used as a criteria to stop recursion in
;        evaluating R
; 
; OUTPUTS:
; R [rad] such that z+R is the true zenith angle or, if /true is set,  z-R
; is the observed zenith angle
;
; OPTIONAL OUTPUTS:
;
; COMMON BLOCKS:
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;
; PROCEDURE:
; CALLS:
;        <a href="refrac_all_p.html#aref" >aref </a>, <a href="refrac_all_p.html#refrac_laclare" >refrac_laclare </a>,
;        <a href="refrac_all_p.html#refrac_for" >refrac_for </a>, <a href="reduced_height.html" >reduced_height </a>,
;        <a href="curvature_radius.html" >curvature_radius </a>,  <a href="air_index.html#air_index" >air_index </a>
;
; EXAMPLE:
;
; MODIFICATION HISTORY:
; T. Corbard 06/2012 (corbard@oca.eu)
;
;-
COMPILE_OPT IDL2

FORWARD_FUNCTION refrac_all

;Default values of optional imput parameters
laps_def=0.0065D0           ;K/m
xc_def=450.D0                      ;ppm
Az_def=0.D0                     ;rad N-S

xc=(N_elements(xc) ne 0)?xc:xc_def
laps=(N_elements(laps) ne 0)?laps:laps_def
Az=(N_elements(Az) ne 0)?Az:Az_def

;Iterative scheme to find R if the true zenith distance is given 
;instead of the observed one
eps=(N_elements(true) ne 0)?true:0.D0

IF(eps gt  0.D0 ) then begin
    zt=z
    cur=zt-refrac_all(z,h,T,P,hum,lbd,lat,type,type_ai,type_rc,laps=laps,xc=xc,Az=Az)
    repeat begin
        last=cur
        dr=refrac_all(cur,h,T,P,hum,lbd,lat,type,type_ai,type_rc,laps=laps,xc=xc,Az=Az)
        cur=zt-dr
    endrep until max(abs(last-cur)*180.D0/!DPI*3600.D0) lt eps
    
    R=dr
ENDIF ELSE BEGIN
    if(type ne 'Laclare') then begin
        rc=curvature_radius(lat,h,Az=Az) ;m
        case type_rc of
            'meridian':rc=rc[0]
            'primevertical':rc=rc[1]
            'meancurv':rc=rc[2]
            'dist2center':rc=rc[3]
            'Az':BEGIN
                if(N_elements(Az) eq 0) then BEGIN
                    print,'Az must be given if Az is chosen as radius of curvature type'
                    stop
                endif
                rc=rc[4]
            END
            ELSE:BEGIN
                print,'rc type unknown: ',type_rc
                stop
            END
        endcase
    endif
    
    
    CASE type OF
        'NAO63':BEGIN
            z_deg=z*180.D0/!DPI ;deg
            lbdm=lbd*1.D6       ;micro meter
            lat_deg=lat*180.D0/!DPI ;degree
            EPS=0.0005           ;arcsec
            P_mb=P/100.D0       ;milibars=hPa
            hum_pc=hum/100.D0
            R=aref(Z_deg,h,T,P_mb,hum_pc,lbdm,lat_deg,laps,rc,EPS,xc=xc)*!DPI/180.D0 ;rad
        END
        'Laclare':BEGIN
            z_deg=z*180.D0/!DPI
            lbdm=lbd*1.d6
            t_celcius=T-273.15d0
            P_mm=P*760.d0/101325.D0
            R=refrac_laclare(z_deg,lbdm,t_celcius,hum,P_mm)/3600.d0*!DPI/180.d0
        END
        ELSE: BEGIN ;Formulae involving only refractive index radius of rurvature 
                                ;and reduced height at observer
            
            l=reduced_height([T])
            n=air_index(T,P,hum,lbd,xc=xc,type_ai)
            ;print,type_ai,n
            R=refrac_for(n,l,rc,z,type) ;rad
        END
    ENDCASE
ENDELSE

RETURN,R

END
    

function refrac_all_p,z,lbda,station,meteo,refrac,true=true
;+
; NAME:
; refrac_all_p
;
; PURPOSE:
; Wrapper for the function refrac_all
;
; CATEGORY: 
; Astronomy
;
; CALLING SEQUENCE:
; R=refrac_all_p(z,lbda,station,meteo,refrac[,/true]
;
; INPUTS:
; z: [rad] Observed zenith angle unless /true 
;    is set in which case z is the true zenith angle
;
; lbda: [m] Wavelength
;
; station: Structure (output of  station() function) <br>
;          station.h (elevation above the reference ellipsoid) <br>
;          station.lat (geodetic latitude in rad)
;          
;
; meteo: structure (output of meteo() function) containing <br> 
;         meteo.T [K] temperature <br>
;         meteo.P [Pa] pression <br>
;         meteo.hum [%] in [0-100] humidity <br>
;         meteo.xc [ppm] CO2 concentration (used only if
;         refrac.type_ai is 'Ciddor'<br>
;         meteo.laps [K/m] troposheric laps rate (used only if 
;         refrac.type is 'NAO63')
;
; refrac: structure defining the refraction model (output of
; refrac_model() function) <br>
;          refrac.type in ['NAO63','Laclare','tan','tan3','tan5',
;                           'erfc','Cassini'] <br>
;          refrac.type_rc in ['meridian','primevertical',
;                             'meancurv','dist2center','Az'] <br>
;          refrac.type_ai in ['Baldini', 'Ciddor'] <br>
;          refrac.Az in [0,2*!DPI] (used only if refrac.type_rc is 'Az')
;           
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;  true: If set it indicates that z is the true zenith angle
;
; OUTPUTS:
; R [rad] such that z+R is the true zenith angle or, if /true is set,  z-R
; is the observed zenith angle
;
; OPTIONAL OUTPUTS:
;
; COMMON BLOCKS:
;
; RESTRICTIONS:
; If /true is set iterative scheme gives mili arc seconds precision on
; the results (see value of eps below)
;
; PROCEDURE:
; CALLS <a href="refrac_all_p.html#refrac_all" >refrac_all.pro </a> 
;
; EXAMPLE:
; calern=station('Calern')
; std_atm=meteo()
; std_refrac=refrac_model('NAO63Ciddor')
; zt=70.D0*!DPI/180.D0
; lbda=535.7d-9
; z=zt-refrac_all_p(zt,lbda,calern,std_atm,std_refrac,/true)
; print,z*180.D0/!DPI
; zt=z+refrac_all_p(z,lbda,calern,std_atm,std_refrac)
; print,zt*180.D0/!DPI
;
; 69.956757
; 70.000000
;
; MODIFICATION HISTORY:
; T. Corbard 02/2013 corbard@oca.eu
;
;-
COMPILE_OPT IDL2

eps=(keyword_set(true))?1.d-3:0.D0

R=refrac_all(z,station.h,meteo.T,meteo.P,meteo.hum,lbda,station.lat,$
             refrac.type,refrac.type_ai,refrac.type_rc,$
             laps=meteo.laps,xc=meteo.xc,Az=refrac.Az,true=eps)


return,R

end


