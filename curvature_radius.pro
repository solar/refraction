function curvature_radius,lat,h,Az=Az
;+
; NAME: curvature_radius 
;
; PURPOSE: Compute various estimate of the radius of curvature
;          and distance from geocenter at observer position
;
; CATEGORY: 
; Geophysics
;
; CALLING SEQUENCE:
; radius=curvature_radius(lat,h[,Az=Az])
;
; INPUTS:
; lat: Geodetic latitude at observer [rad]
; h: Elevation of observer above the reference ellipsoid (added to the output) [m]
;
; OPTIONAL INPUTS:
; 
; KEYWORD PARAMETERS:
; Az: Azimuth angle [rad] (default =0)
;
; OUTPUTS:
; radius[0] curvature in the (noth-south) meridian [m] <br>
; radius[1] curvature in the prime vertical [m] <br>
; radius[3] mean radius of curvature (=sqrt(radius[0]*radius[1])) [m] <br>
; radius[4] distance from geocenter (WGS84) [m] <br>
; radius[5] radius of curvature at azimuth angle Az (if set) [m]
;
; OPTIONAL OUTPUTS:
; 
; COMMON BLOCKS:
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;
; PROCEDURE:
; Use WGS84 Reference ellipsoid a=6378137 m, b=6356752 m <br>
;\begin{equation}r_c^0={(ab)^2 \over
;{\left(a^2\cos^2(\varphi)+b^2\sin^2(\varphi) \right)^{3/2}}}
;\end{equation}<br><br>
;
;\begin{equation}
;r_c^{90}={a^2 \over {\sqrt{a^2\cos^2(\varphi)+b^2\sin^2(\varphi)} }}
;\end{equation}<br><br>
;
; \begin{equation}
;<\!\!r_c\!\!>=\sqrt{r_c^0 r_c^{90}}= {a^2b \over
;{a^2\cos^2(\varphi)+b^2\sin^2(\varphi) }}
;\end{equation}<br><br>
;
;\begin{equation}
;R=\sqrt{ {{a^4\cos^2(\varphi)+b^4\sin^2(\varphi)} }\over
;{{a^2\cos^2(\varphi)+b^2\sin^2(\varphi)} }}
;\end{equation} <br><br>
;
;\begin{equation}
;r_c^A={1 \over {{\cos^2(A)\over r_c^0}+{\sin^2(A)\over r_c^{90}}  } }
;\end{equation}
;
; EXAMPLE:
;
; MODIFICATION HISTORY:
; T. Corbard 06/2012 corbard@oca.eu
;
;-
COMPILE_OPT IDL2

;WGS84 reference ellipsoid
a=6378137.D0 ;[m]
b=6356752.D0 ;[m]

a2=a*a
b2=b*b

sl2=sin(lat)^2
cl2=cos(lat)^2


c=a2*cl2+b2*sl2
r0=a2*b2/c^(1.5) 
r90=a2/sqrt(c)   
rm=sqrt(r0*r90)  
Rd=sqrt((a2*a2*cl2+b2*b2*sl2)/c)

if (N_elements(Az) ne 0) then begin
    rA=1.D0/(cos(Az)^2/r0+sin(Az)^2/r90) 
    return,[r0,r90,rm,Rd,rA]+h
endif else begin
    return,[r0,r90,rm,Rd]+h
endelse

end
