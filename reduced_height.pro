function reduced_height,arg
;+
; NAME: 
; reduced_height
;
; PURPOSE: 
; Give the "reduced height" or height of an homogenous atmosphere 
; with the same density as the density at observer position and that
; would give the same pressure as the one recorded at observer position.
;
; CATEGORY:
;  Atmospheric Physics
;
; CALLING SEQUENCE:
; l=reduced_height([T]) 
; l=reduced_height([P,rho,g])
;
; INPUTS:
; arg: 1 or 3 element vector <br>
;      arg[0] : Temperature [K]  or 
;               Pressure [Pa] (if arg[1:2] are defined) <br> 
;      arg[1]: Density [kg/m$^3$] (optional)<br>
;      arg[2]: gravity [m/s$^2$] (optional)  <br>
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;
; OUTPUTS: 
; homogeneous atmosphere reduced height [m]
;
; OPTIONAL OUTPUTS:
;
; COMMON BLOCKS:
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;
; PROCEDURE:
;  \begin{equation}
;  \ell(P,\rho,g)={P \over {\rho \ g}}
; \end{equation}
; If only temperature is given, ideal gas law for dry air
; is assumed. 
; \begin{equation}
;\ell(T)={P_0 \over {\rho_0 \ g_0}}{T \over T_0}
;\end{equation}
;where ${\rho_0=1.293\ \mbox{kg}\,\mbox{m}^{-3}}$ for
;${T_0=273.15\,\mbox{K}}$, ${P_0=101325\,\mbox{Pa}}$ and normal
;gravity ${g_0=9.80665\,\mbox{m}\,\mbox{s}^{-2}}$. 
;
; EXAMPLE:
; ;Compare reduced height assuming ideal gaz law or from full
; ;air density calculation
;
;t=15.D0 ;Celsius
;P=875d2 ;Pa
;hum=50.D0 ;%
;xc=380 ; ppm
;g=9.802155492d0   ; m/s^2 Measured gravity acceleration at Calern
; ;Get the air density from Ciddor (1996) equations 
;d=atmospheric_density(t,P,hum,xc)
;print,'Air Density: ',d,' kg/m^2',format='(a,d5.3,a)'
;print,'Reduced height (Full density calculation): ',round(reduced_height([P,d,g])),' m' 
;print,'Reduced height (Assuming ideal gaz law  ): ',round(reduced_height([t+273.15D0])),' m'
;
;Air Density: 1.054 kg/m^2
;Reduced height (full density calculation):      8467 m
;Reduced height (assuming ideal gaz law  ):      8430 m
;
; MODIFICATION HISTORY:
; T. Corbard 06/2012 corbard@oca.eu
;
;-

l=0
CASE N_elements(arg) of 
1: BEGIN
    Ta=arg[0] ;K
    T0=273.15d0 ;K
    P0=101325.D0 ;Pa
    g0=9.80665 ;m s^-2
    rho0=1.293 ;kg m^-3
    l=P0/(rho0*g0)*Ta/T0 ;m 
END
3: BEGIN
    P=arg[0] ;Pa
    rho=arg[1] ;kg m^-3
    g=arg[2] ;m s^-1
    l=P/(rho*g) ;m
END
else: message,'unvalid argument'
ENDCASE

return,l
end
